FROM node:16-alpine3.11

WORKDIR /timeTracking/src/app

COPY package*.json ./

RUN yarn install

COPY . .

RUN yarn run build

CMD [ "node", "dist/main.js" ]
EXPOSE 3000


