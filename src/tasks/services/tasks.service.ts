import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TaskEntity } from '../entities/task.entity';
import { CreateTaskDto } from '../dto/create-task.dto';
import { TaskDto } from '../dto/task.dto';

@Injectable()
export class TasksService {
    constructor(
        @InjectRepository(TaskEntity)
        private readonly tasksRepository: Repository<TaskEntity>
    ) {}

    async getCurrent(): Promise<TaskDto> {
        let task = await this.tasksRepository.createQueryBuilder().orderBy('id' ,'DESC').getOne();
        if (task !== undefined && task.end_time === null) { return task }
        throw new BadRequestException('task does not exist.')
    }

    async stop(): Promise<TaskDto> {
        let result = await this.stopTask();
        if (result === null){
            throw new BadRequestException('task is already stopped.')
        }
        return result;
    }

    async create(task: CreateTaskDto): Promise<TaskDto> {
        await this.stopTask();
        return this.tasksRepository.save(task);
    } 

    async stopTask(): Promise<TaskDto> {
        let task = await this.tasksRepository.createQueryBuilder().orderBy('id' ,'DESC').getOne();
        if (task !== undefined && task.end_time === null) {
            return this.tasksRepository.save({id: task.id, end_time: new Date() })
        }
        return null
    }
}
