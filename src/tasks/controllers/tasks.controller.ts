import { Body, Controller, Get, Post } from '@nestjs/common';
import { TasksService } from '../services/tasks.service';
import { ApiBadRequestResponse, ApiBody, ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { CreateTaskDto } from '../dto/create-task.dto';
import { TaskDto } from '../dto/task.dto';

@ApiTags('tasks')
@Controller('tasks')
export class TasksController {
    constructor(private taskService: TasksService) {}

    @Get('current')
    @ApiOperation({ description: 'Fetch users current running task.' })
    @ApiOkResponse({ description: 'Get current task', type: TaskDto })
    @ApiBadRequestResponse({ description: 'Task does not exist.' })

    getUserTask(): Promise<TaskDto> {
        return this.taskService.getCurrent();
    }

    @Post()
    @ApiOperation({ description: 'Create new task and end currently running one if exist.' })
    @ApiCreatedResponse({ description: 'New task has been sucessfully created and old task sucessfully updated' })
    @ApiBadRequestResponse({ description: 'name must be a string'})
    @ApiBody({ type: CreateTaskDto })

    create(@Body() task: CreateTaskDto): Promise<TaskDto> {
        return this.taskService.create(task);
    }

    @Post('stop')
    @ApiOperation({ description: 'Stop curently running task.' })
    @ApiCreatedResponse({ description: 'Current task has been sucessfully stopped' })
    @ApiBadRequestResponse({ description: 'Task is already stopped' })
    
    stop(): Promise<TaskDto> {
        return this.taskService.stop();
    }
}