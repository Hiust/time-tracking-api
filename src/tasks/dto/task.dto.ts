import { ApiProperty } from "@nestjs/swagger";

export class TaskDto {
    @ApiProperty()
    public id: number;

    @ApiProperty()
    public name: string;

    @ApiProperty()
    public start_time: Date;

    @ApiProperty()
    public end_time?: Date;
}