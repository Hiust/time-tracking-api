import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('task')
export class TaskEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP'})
    start_time: Date;

    @Column({ type: 'timestamptz', nullable: true })
    end_time: Date;
}