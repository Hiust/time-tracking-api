import { HttpException, NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { TaskDto } from '../dto/task.dto';
import { TaskEntity } from '../entities/task.entity';
import { TasksService } from '../services/tasks.service';

describe('TasksService', () => {
  let service: TasksService;

  const stopTask = jest.spyOn(TasksService.prototype as any, 'stopTask');
  const getOneSpy = jest.fn()
  .mockReturnValueOnce(undefined)
  .mockReturnValueOnce({
    id: 1,
    name: 'TaskName',
    start_time: new Date(),
    end_time: null
  })
  .mockReturnValueOnce({
    id: 1,
    name: 'TaskName',
    start_time: new Date(),
    end_time: null
  })
  

  const mockTaskRepository = {
    save: jest.fn().mockImplementation(dto => {
        if (dto.id === undefined) dto.id = 1
        if (dto.end_time === undefined) dto.end_time = null
        if (dto.start_time === undefined && dto.name === undefined)
        {
          return {
            id:dto.id,
            end_time: new Date()
          }
        }
        
        return {
          id: dto.id,
          name: dto.name,
          start_time: new Date(),
          end_time: dto.end_time
        };
      }
    ),

    createQueryBuilder: jest.fn(() => ({
      orderBy: jest.fn().mockReturnThis(),
      getOne: getOneSpy,
    })),
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TasksService,
        {
          provide: getRepositoryToken(TaskEntity),
          useValue: mockTaskRepository
        },
      ],
    }).compile();

    service = module.get<TasksService>(TasksService);
  });

  it('should fetch current task',async () => {
    expect(service.getCurrent()).rejects.toThrowError(NotFoundException);
    expect(await service.getCurrent()).toEqual({
      id: expect.any(Number),
      name: expect.any(String),
      start_time: expect.any(Date),
      end_time: null
    });
  });

  it('should stop current task and return null or task //helper', async () =>
  {
    expect(await service.stopTask()).toEqual({
      id: expect.any(Number),
      end_time: expect.any(Date)
    });
    expect(await service.stopTask()).toEqual(null);
    
  })

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create and stop current task', async () => {
    stopTask.mockImplementation(() => {}).mockReturnValueOnce(null).mockReturnValueOnce({
      id: 1,
      end_time: Date()
    });

    expect(await service.create({name : 'TaskName'})).toEqual({
      id: expect.any(Number),
      name: 'TaskName',
      start_time: expect.any(Date),
      end_time: null
    })

    expect(await service.create({name : 'TaskName'})).toEqual({
      id: expect.any(Number),
      name: 'TaskName',
      start_time: expect.any(Date),
      end_time: null
    })

    expect(mockTaskRepository.save).toHaveBeenCalled()
    expect(stopTask).toBeCalled();
  });

  it('should stop current task', async () => {
    stopTask.mockImplementation(() => {}).mockReturnValueOnce(null).mockReturnValueOnce({
      id: 1,
      end_time: new Date()
    });
    

    await expect(service.stop()).rejects.toThrowError(HttpException)

    expect(await service.stop()).toEqual({
      id: expect.any(Number),
      end_time: expect.any(Date)
    });
    expect(stopTask).toBeCalled();
  });
});
