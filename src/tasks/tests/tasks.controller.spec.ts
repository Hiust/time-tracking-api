import { Test, TestingModule } from '@nestjs/testing';
import { TasksService } from '../services/tasks.service';
import { TasksController } from '../controllers/tasks.controller';

describe('TaskController', () => {
  let controller: TasksController;

  const mockTasksService = {
    stop: jest.fn().mockReturnValueOnce(
      {
        id: 1,
        end_time: new Date()
      }
    ).mockReturnValueOnce(
      {
        statusCode: 206,
        message: "task is already stopped."
      }
    ),

    create: jest.fn(dto => {
      return {
        id: 1,
        ...dto,
        start_time: new Date(),
        end_time: null
      }
    }),

    getCurrent: jest.fn().mockReturnValueOnce(
      {
        statusCode: 404,
        message: "task does not exist."
      }
    ).mockReturnValueOnce(
      {
        id: 1,
        name: 'TaskName',
        start_time: new Date(),
        end_time: null
      }
    )
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TasksController],
      providers: [TasksService],
    }).overrideProvider(TasksService).useValue(mockTasksService).compile();

    controller = module.get<TasksController>(TasksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create a task and stop last task', () => {

    const taskDto = {name: 'TaskName'};

    expect(controller.create(taskDto)).toEqual({
      id: expect.any(Number),
      name: 'TaskName',
      start_time: expect.any(Date),
      end_time: null
    });

    expect(mockTasksService.create).toHaveBeenLastCalledWith(taskDto);
  });

  it('should stop a task or throw 206 status code', () => {
    expect(controller.stop()).toEqual({
      id: expect.any(Number),
      end_time: expect.any(Date)
    });

    expect(controller.stop()).toEqual({
      statusCode: expect.any(Number),
      message: expect.any(String)
    });

    expect(mockTasksService.stop).toHaveBeenCalledTimes(2);
  })

  it('should fetch current task', () => {
    expect(controller.getUserTask()).toEqual({
      statusCode: expect.any(Number),
      message: expect.any(String)
      });

    expect(controller.getUserTask()).toEqual({
      id: expect.any(Number),
      name: expect.any(String),
      start_time: expect.any(Date),
      end_time: null
    });

    expect(mockTasksService.stop).toHaveBeenCalledTimes(2);
  })
});