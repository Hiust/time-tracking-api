# TimeTrackingApi
An API for time tracking.

Used tools:
- NestJs
- TypeOrm
- Swagger
- Postgresql
- Docker

## Running the app
### Locally
```bash
# install dependencies
$ yarn install

# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```
### Docker
```bash
# to run
$ docker-compose up -d

# to stop
$ docker-compose stop
```

## Documentation
documentation can be found at http://localhost:3000/api

## Test
### Locally
```bash
$ yarn run testStmts

$ yarn run test:e2e

$ yarn run test:cov
```

### Docker
```bash
$ docker exec -it api yarn run test

$ docker exec -it api yarn run test:e2e

$ docker exec -it api yarn run test:cov
```