import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { TaskModule } from '../src/tasks/tasks.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { getConnection } from 'typeorm';

describe('TaskController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({ isGlobal: true}),
        TypeOrmModule.forRoot({
          type: 'postgres',
          url: process.env.TEST_DATABASE_URL,
          autoLoadEntities: true,
          synchronize: true,
        }),
        TaskModule
      ],
    }).compile();
    app = moduleFixture.createNestApplication(); 
    app.useGlobalPipes(new ValidationPipe());
    await getConnection().synchronize(true);
    await app.init();
  });

  afterAll(async () => {
    await getConnection().synchronize(true);
    await app.close()
  });

  const createTask = () => {
    return request(app.getHttpServer())
    .post('/tasks')
    .send({ name: 'NiceName' })
    .expect(201)
    .then((response) => {
      expect(response.body).toEqual({
        id: expect.any(Number),
        name: expect.any(String),
        start_time: expect.any(String),
        end_time: null
      });
    });
  }

  const checkEndTimeValue = async () => {
    let lastTask = await getConnection().query('SELECT * FROM task LIMIT 1');
    expect(lastTask[0].end_time).not.toBeNull();
  }

  describe('Start tracking new task and stop previosly task', () => {
    beforeAll(async () => { await getConnection().synchronize(true); });

    it('create task1', async () => { return await createTask() });
    it('create task2', async () => { return await createTask() });
    it('check value of last tast', async () => { await checkEndTimeValue(); });
  });

  describe('Stop running task at any moment', () => {
    beforeAll(async () => { await getConnection().synchronize(true); });
    it('create task1', async () => { return await createTask() });
    
    it('stop running task', async () => {
      const response = await request(app.getHttpServer()).post('/tasks/stop').expect(201);
      expect(response.body).toEqual({
        id: expect.any(Number),
        end_time: expect.any(String)
      })
    });

    it('check value of last tast', async () => { await checkEndTimeValue(); });
  });

  describe('fetch current running task', () => {
    beforeAll(async () => {
      await getConnection().synchronize(true);
    });

    it('create task', async () => { return await createTask() });
    it('fetch current Task', () => {
        return request(app.getHttpServer()).get('/tasks/current').expect(200)
        .then((response) => {
          expect(response.body).toEqual({
            id: expect.any(Number),
            name: expect.any(String),
            start_time: expect.any(String),
            end_time: null
          })
        })
      });
  });
});